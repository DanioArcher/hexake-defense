# 1.0.0.0 (4-06-2021)

### Added:
- Power Ups - you can pick up these and use to disturb another player, e.g. decrease his speed
- Two new maps with obstacle tiles
- Map selector in main menu
- Power ups are being displayed randomly on the map
- Delay time for pickable tiles appearing
- Counting players score (how many times they won)
- Score points can be reset with reset score button
- New look of power ups
- Music and sound effects
- If there is no connected controller, player can not start the game. Also while playing, game also checks if controller is still connected
- For active power ups you can see what kind of power up it is and what is his duration time
- Collected power up is displayed in the right corner with both name and description

### Changed:
- New look of main menu
- A lot of code improvements (e.g. inheritance for all tiles which can be picked up)
- Instead of two game object panels (Win and Death screen), now there is one panel with changing text

### Fixed:
- UI now shows properly on different resolutions and aspect ratios
- Sometimes after end of power ups, snake did not get back to his default settings
- Player could pause the game during countdown

# 0.6.2.3 (15-05-2021)

### Added
- Ability to boost snake - increase his movement speed for a small period of time
- Fade in and fade out animation for building resources which appears on the map
- Displaying a message for player to inform him why he can't build a turret

### Changed
- In-game font
- New main menu design
- Increased text space for displaying quantity of resources

# 0.5.0.0 (01-05-2021)

### Added
- More turrets
- Boundaries of a map, if player touch it, he will die
- Bullets can do damage to snake's head
- Showing turret image with his health bar and snake's head with his health bar on top of the screen
- Countdown before game begins
- Loading Screen
- Information on how to control a player
- Glow (bloom) effect for snake's head and tail
- Universal Render Pipeline

### Changed
- Canvases for both players now have render type set to Screen Space Camera
- Win and Death screens that are displayed for players
- End game screen with restart and main menu buttons

### Fixed
- Collecting bronze and stone did not add this resources to the player

# 0.1.0.1 (20-02-2021)

### Added
- Detecting collision between players (without damage) and when collision occurs, Inviolability will be enabled for small amount of time
- Bullets do damage to turrets
- Turrets flash when taking damage

### Changed
- Sorting Layer for Bullet, Bullet Trail and Player

# 0.1.0.0 (10-01-2021)

### Added

- Split Screen
- Smooth snake movement
- Turrets can shoot bullets
- Turrets can rotate
- Snake can gather resources
- Player can open shop and buy turrets