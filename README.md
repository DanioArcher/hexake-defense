# Hexake Defense

Simple and first Unity completed game written in C#. Game requires a controller to play. The best choice is to choose an Xbox controller (the game was tested on Xbox 360 controller, but Xbox One controller should also be working). 

You are moving with a snake, but on hex grid. You can collect resources for building turrets to shot another player. There is also some types of power ups which you can use. Your goal is to destroy another player and do not get yourself killed by him.

Download link (You will need a controller!): https://drive.google.com/drive/folders/1V1i84ooSxhMfMCJ2Afy6wnBRn-ynhqQ2?usp=sharing

![image_1](image_1.png)

![image_2](image_2.png)

![image_3](image_3.png)

![image_4](image_4.png)
