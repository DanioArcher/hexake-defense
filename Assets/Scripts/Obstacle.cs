using UnityEngine;

/*
 * Script for obstacle game object on the map
 */

public class Obstacle : MonoBehaviour
{
    public float onHitDamage = 40f; //what damage will player get when he will hit an obstacle

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.TryGetComponent(out IDamageable player))
        {
            collision.GetComponent<Head>().SetOnCollisionDamage();
            player.DealDamage(onHitDamage);
        }
    }
}
