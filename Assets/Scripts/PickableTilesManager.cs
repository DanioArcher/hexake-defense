using UnityEngine;
using UnityEngine.Tilemaps;
using System.Collections;

/*
 * PickableTiles are game objects that you can pick up on a map (e.g. building resource or power up)
 */

public class PickableTilesManager : MonoBehaviour
{
    public const float DEFAULT_POWERUP_TIME = 10f; //after what time, a new power up should be spawned

    /*
     * array index 0 - small amount of resources (10)
     * array index 1 - medium amount of resources (20)
     * array index 2 - big amount of resources (30)
     */
    public GameObject[] wood;
    public GameObject[] stone;
    public GameObject[] bronze;

    //storing a different power ups
    public GameObject[] powerUps;

    private Tilemap tilemap;

    public enum TileType { wood, stone, bronze, powerUp };

    private void Awake()
    {
        tilemap = FindObjectOfType<Tilemap>();
        CreateResource(TileType.wood);
        CreateResource(TileType.stone);
        CreateResource(TileType.bronze);
        CreateResource(TileType.powerUp);
    }

    public void CreateResource(TileType type, float createAfterTime = 1f)
    {
        bool repeat = true;
        int x, y;
        Vector3Int pos;

        while (repeat)
        {
            //TEMPORARY SOLUTION
            x = Random.Range(-14, 8); //Random.Range(-tilemap.size.x / 2, tilemap.size.x / 2);
            y = Random.Range(-8, 7); //Random.Range(-tilemap.size.y / 2, tilemap.size.y / 2);
            pos = new Vector3Int(x, y, 0);

            //if on this random position, an object exists, then randomize another new position
            if (!Physics2D.Raycast(new Vector2(x, y), Vector2.zero))
            {
                switch (type)
                {
                    case TileType.wood:
                        StartCoroutine(SpawnResource(wood[RandomResourceIndex()], pos, createAfterTime));
                        break;
                    case TileType.stone:
                        StartCoroutine(SpawnResource(stone[RandomResourceIndex()], pos, createAfterTime));
                        break;
                    case TileType.bronze:
                        StartCoroutine(SpawnResource(bronze[RandomResourceIndex()], pos, createAfterTime));
                        break;
                    case TileType.powerUp:
                        StartCoroutine(SpawnResource(powerUps[RandomPowerUpIndex()], pos, createAfterTime));
                        break;
                }

                repeat = false;
            }
        }
    }

    private int RandomResourceIndex() //randomize amount of resources to spawn (for wood, stone and bronze)
    {
        float randomType = Random.Range(0f, 1f);
        return (randomType > 0 && randomType < 0.5f) ? 0 : (randomType > 0.5f && randomType < 0.8f) ? 1 : 2;
    }

    private int RandomPowerUpIndex() //random index of power up to create
    {
        return Random.Range(0, powerUps.Length);
    }

    public IEnumerator SpawnResource(GameObject resource, Vector3Int pos, float createAfterTime)
    {
        yield return new WaitForSeconds(createAfterTime);
        Instantiate(resource, tilemap.CellToWorld(pos), Quaternion.identity, transform);
    }
}
