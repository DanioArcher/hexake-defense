using UnityEngine;
using System.Collections;

/*
 * PickableTile is a game object that you can pick up on a map (e.g. building resource or power up)
 */

public class PickableTile : MonoBehaviour
{
    protected PickableTilesManager resourceManager;

    protected virtual void Awake()
    {
        resourceManager = FindObjectOfType<PickableTilesManager>();

        StartCoroutine(FadeIn());
    }

    protected IEnumerator FadeIn() //creating animation when object is being instantiated
    {
        SpriteRenderer resource = GetComponent<SpriteRenderer>();

        int alpha = 0;
        Color32 newColor = resource.color;
        newColor.a = (byte)alpha;

        while (alpha <= 255)
        {
            resource.color = newColor;

            alpha += 5;
            newColor.a = (byte)alpha;

            yield return null;
        }
    }

    protected IEnumerator FadeOut() //destroying animation when was picked up
    {
        GetComponent<CircleCollider2D>().enabled = false; //head has OnTriggerStay function
        SpriteRenderer resource = GetComponent<SpriteRenderer>();

        int alpha = 255;
        Color32 newColor = resource.color;
        newColor.a = (byte)alpha;

        while (alpha >= 0)
        {
            resource.color = newColor;

            alpha -= 20;
            newColor.a = (byte)alpha;

            yield return null;
        }

        DestroyThisGameObject();
    }

    protected virtual void DestroyThisGameObject()
    {
        Destroy(gameObject);
    }
}
