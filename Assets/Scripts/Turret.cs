﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/*
 * Script for turret game object
 */

public class Turret : MonoBehaviour, IDamageable
{
    public GameObject bullet; //reference to bullet prefab
    private Transform[] bulletSpawnPos; //position, where to spawn a bullets

    private IEnumerator damageBlink; //when courutine is in variable we can use StopCoroutine

    public float maxHealth = 100f;
    private float health;
    private float Health
    {
        get { return health; }
        set
        {
            if (health > value) //if turret lost some amount of health
            {
                if (damageBlink != null) 
                {
                    StopCoroutine(damageBlink);
                    GetComponent<SpriteRenderer>().material = originalMaterial;
                }
                damageBlink = DamageBlink();
                StartCoroutine(damageBlink);
            }
            
            health = value;
            if(healthPanel) 
                healthPanel.transform.GetChild(0).GetChild(0).GetComponent<Image>().fillAmount = Health / maxHealth;

            if (health <= 0)
            {
                healthPanel.SetActive(false);
                Destroy(gameObject);
            }
        }
    }
    public float damage = 5f;
    public float rotationTime = 5f;
    public float bulletSpeed = 3f;
    public float reloadTime = 1f;
    private float reloadTimer;

    public int woodCost;
    public int stoneCost;
    public int bronzeCost;

    private float currentRotateValue; //In Unity, rotate values in inspector are different than rotate values taken from script so we need our own variable for rotation
    private bool isRotating = false; //if turret is rotating it can not shoot

    private PlayerManager playerManager; //reference to read controls type (keyboard or joystick)

    private Material originalMaterial;
    private Material blinkMaterial;

    private GameObject healthPanel; //reference to an object which shows health for the turret

    private void Awake()
    {
        bulletSpawnPos = new Transform[transform.childCount];

        for (int i = 0; i < transform.childCount; i++)
            bulletSpawnPos[i] = transform.GetChild(i);

        reloadTimer = reloadTime;
        Health = maxHealth;

        transform.eulerAngles = new Vector3(0, 0, transform.root.GetComponent<Movement>().turnValue); //get current rotate value
        currentRotateValue = transform.root.GetComponent<Movement>().turnValue; //get current rotate value at start as default value

        playerManager = transform.root.GetComponent<PlayerManager>();

        originalMaterial = GetComponent<SpriteRenderer>().material;
        blinkMaterial = Resources.Load<Material>("Materials/DamageBlink");
    }

    private void Update()
    {
        #region Rotate turret to look at mouse
        //mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //mousePos.z = 0;
        //transform.up = Vector3.Lerp(transform.up, (mousePos - transform.position).normalized, rotationTime * Time.deltaTime);
        #endregion

        if (reloadTimer <= reloadTime) 
            reloadTimer += Time.deltaTime;

        if (reloadTimer >= reloadTime && !isRotating && !playerManager.disableTurretShoot)
            if((Input.GetKeyDown(KeyCode.Space) && playerManager.controls.Equals(PlayerManager.Controls.keyboard)) 
            || (Input.GetKey(KeyCode.Joystick1Button0) && playerManager.controls.Equals(PlayerManager.Controls.joystick)))
            {
                for(int i = 0; i < bulletSpawnPos.Length; i++)
                {
                    GameObject myBullet = Instantiate(bullet, bulletSpawnPos[i].position, Quaternion.identity);
                    myBullet.GetComponent<Rigidbody2D>().velocity = transform.up * bulletSpeed;
                    myBullet.GetComponent<Bullet>().myTurret = this.gameObject;
                    myBullet.transform.up = transform.up;
                    myBullet.layer = gameObject.layer;
                    reloadTimer = 0f;
                }
            }
    }

    public void SetHealthPanel(GameObject panel) //when object is instantiated set his panel which was assigned during creating turret process
    {
        healthPanel = panel;
        healthPanel.GetComponent<Image>().sprite = GetComponent<SpriteRenderer>().sprite;
        healthPanel.transform.GetChild(0).GetChild(0).GetComponent<Image>().fillAmount = health / maxHealth;
    }
    
    public void startRotateTurret(int direction)
    {
        StopAllCoroutines(); //when rotateTurret coroutine has not ended and we call for another, then stop this coroutine to prevent from doubling functions (two same coroutines in one time)
        StartCoroutine(rotateTurret(direction));
    }

    public IEnumerator rotateTurret(int direction)
    {
        isRotating = true;

        while (currentRotateValue != transform.root.GetComponent<Movement>().turnValue) //repeat until this turret rotate value is not equal to global rotate turrets value
        {
            transform.eulerAngles = new Vector3(0, 0, currentRotateValue);
            currentRotateValue = currentRotateValue >= 390f ? 30f : currentRotateValue <= -390f ? -30f : currentRotateValue; //prevent from exceeding the range of variable
            currentRotateValue += direction * rotationTime;

            yield return null;
        }

        //when rotation has ended overwrite current rotate value on this turret to global rotate turrets value to prevent from small differences in values
        currentRotateValue = transform.root.GetComponent<Movement>().turnValue; 
        transform.eulerAngles = new Vector3(0, 0, currentRotateValue);
        isRotating = false;
    }

    private IEnumerator DamageBlink() //turret blink when receiving damage
    {
        GetComponent<SpriteRenderer>().material = blinkMaterial;
        yield return new WaitForSeconds(0.1f);
        GetComponent<SpriteRenderer>().material = originalMaterial;
    }


    public void DealDamage(float damage) => Health -= damage;
}
