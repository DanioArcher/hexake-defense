﻿using UnityEngine;
using System.Collections;

/*
 * Script for bullet game object
 */

public class Bullet : MonoBehaviour
{
    private float lifeTime = 5f; //after this time, if bullet did not hit anything, destroy it
    private float time = 0f; //count time

    [HideInInspector] public GameObject myTurret; //stores reference to turret from which this bullet came to define friendly and enemy turrets

    private void Awake() => StartCoroutine(LifeTime());

    private IEnumerator LifeTime()
    {
        while(time <= lifeTime)
        {
            time += Time.deltaTime;
            yield return null;
        }

        Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.GetComponent<IDamageable>() != null)
        {
            collision.GetComponent<IDamageable>().DealDamage(myTurret.GetComponent<Turret>().damage);
            Destroy(this.gameObject);
        }
        else if(collision.CompareTag("Obstacle"))
            Destroy(this.gameObject);
    }
}
