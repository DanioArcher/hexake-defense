using UnityEngine;
using UnityEngine.UI;

/*
 * Script for selecting a map in a main menu
 */

public class MapSelect : MonoBehaviour
{
    private Image mapImage;
    private const int maxMapCount = 3; //how many maps are in the game

    //static, because we need to pass that information to another scene (to set a correct map)
    [HideInInspector] public static int currentSelectedMap { get; private set; }
    private int CurrentSelectedMap
    {
        get { return currentSelectedMap; }
        set
        {
            currentSelectedMap = value;
            SetMapImage();
        }
    }

    public Sprite[] mapsImages;

    private void Awake()
    {
        mapImage = transform.Find("MapImage").GetComponent<Image>();
        CurrentSelectedMap = 0;
    }

    public void GoLeft() //left arrow (button)
    {
        if(CurrentSelectedMap > 0 && CurrentSelectedMap < maxMapCount)
            CurrentSelectedMap--;
    }

    public void GoRight() //right arrow (button)
    {
        if (CurrentSelectedMap >= 0 && CurrentSelectedMap < maxMapCount - 1)
            CurrentSelectedMap++;
    }

    private void SetMapImage()
    {
        mapImage.sprite = mapsImages[CurrentSelectedMap];
    }
}
