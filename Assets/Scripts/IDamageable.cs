﻿/*
 * Script for each object that can be damaged and has a health
 */

public interface IDamageable
{
    void DealDamage(float dmg);
}
