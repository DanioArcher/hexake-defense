using System.Collections;
using UnityEngine;

public class DisableSnakeControlPU : PowerUp
{
    protected override IEnumerator Effect(GameObject enemy)
    {
        Movement enemyMovement = enemy.GetComponent<Movement>();

        enemyMovement.DisableSnakeControl(true);
        yield return new WaitForSeconds(durationTime);
        enemyMovement.DisableSnakeControl(false);

        DestroyPowerUp();
    }
}
