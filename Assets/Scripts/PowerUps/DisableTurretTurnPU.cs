using System.Collections;
using UnityEngine;

public class DisableTurretTurnPU : PowerUp
{
    protected override IEnumerator Effect(GameObject enemy)
    {
        Movement enemyMovement = enemy.GetComponent<Movement>();

        enemyMovement.DisableTurretTurn(true);
        yield return new WaitForSeconds(durationTime);
        enemyMovement.DisableTurretTurn(false);

        DestroyPowerUp();
    }
}
