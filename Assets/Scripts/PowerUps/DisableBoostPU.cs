using System.Collections;
using UnityEngine;

public class DisableBoostPU : PowerUp
{
    protected override IEnumerator Effect(GameObject enemy)
    {
        Movement enemyMovement = enemy.GetComponent<Movement>();

        enemyMovement.DisableBoost(true);
        yield return new WaitForSeconds(durationTime);
        enemyMovement.DisableBoost(false);

        DestroyPowerUp();
    }
}
