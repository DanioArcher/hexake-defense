using UnityEngine;
using System.Collections;

public class DecreaseSpeedPU : PowerUp
{
    public float moveSpeedValue = .5f;

    protected override IEnumerator Effect(GameObject enemy)
    {
        Movement enemyMovement = enemy.GetComponent<Movement>();
        float defaultMoveSpeed = enemyMovement.moveSpeed; //save previous move speed value before changing it

        enemyMovement.ChangeMoveSpeed(moveSpeedValue);
        yield return new WaitForSeconds(durationTime);
        enemyMovement.ChangeMoveSpeed(defaultMoveSpeed);

        DestroyPowerUp();
    }
}
