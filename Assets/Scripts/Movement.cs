﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Tilemaps;

/*
 * Snake can move only one block left, straight or right
 */

public class Movement : MonoBehaviour
{
    private Tilemap basic;
    private Transform head; //save head, used for calculating in which direction snake is moving
    private Transform mainBody; //first part of tail, used for calculating in which direction snake is moving
    private Transform bodies; //transform of game object where are all body parts
    private Vector3Int middleTile; //tile in front of head snake, stores tile to which snake can move
    private Vector3Int leftTile; //tile to the left of middleTile, stores tile to which snake can move
    private Vector3Int rightTile; //tile to the right of middleTile, stores tile to which snake can move
    private int moveTileSide = 0; //says where is middleTile, 1 - vertical right | -1 - vertical left | 2 - down right | -2 - down left | 3 - top right | -3 - top left

    private float defaultMoveSpeed = 1f;
    public float moveSpeed = 1f; //how fast snake is moving
    public float boostMoveSpeed = 2f;
    public float boostFillSpeed = 0.05f;
    public float boostTime = 5f;
    private bool isBoostActive = false; //tells if boost was activated and is still in progress (needed for changing moveSpeed only when snake has reached target tile)
    private Vector3Int moveTo; //position to which head will be moved
    private bool isMoving = false; //says if head is moving at the moment

    //variables needed for power ups
    private bool hasMoveSpeedChanged = false;
    private float moveSpeedChanged = 1f;
    private bool disableBoost = false;
    private bool disableTurretTurn = false;
    private bool disableSnakeControl = false;

    private enum Direction { none, left, right };
    private Direction myDirection;
    
    private PlayerManager playerManager; //reference to read controls type (keyboard or joystick)

    public Image boostBar;

    [HideInInspector] public float turnValue = -90f; //saves current turrets rotate value

    private void Awake()
    {
        basic = GameObject.FindGameObjectWithTag("Basic").GetComponent<Tilemap>();
        head = transform.GetChild(0).GetComponent<Transform>();
        bodies = transform.GetChild(1).transform;
        mainBody = bodies.GetChild(0).transform;

        playerManager = transform.root.GetComponent<PlayerManager>();

        boostBar.fillAmount = 0;
    }

    private void Update()
    {
        if(!disableSnakeControl)
        {
            if ((Input.GetKeyDown(KeyCode.A) && playerManager.controls.Equals(PlayerManager.Controls.keyboard))
                || (Input.GetAxisRaw("JoystickLeftStick") < 0 && playerManager.controls.Equals(PlayerManager.Controls.joystick))) myDirection = Direction.left;
            else if ((Input.GetKeyDown(KeyCode.D) && playerManager.controls.Equals(PlayerManager.Controls.keyboard))
                || (Input.GetAxisRaw("JoystickLeftStick") > 0 && playerManager.controls.Equals(PlayerManager.Controls.joystick))) myDirection = Direction.right;
        }

        if (Input.GetKeyDown(KeyCode.LeftShift) && boostBar.fillAmount >= 1)
            if (Input.GetKeyDown(KeyCode.LeftShift) && playerManager.controls.Equals(PlayerManager.Controls.keyboard)
                || (Input.GetKeyDown(KeyCode.Joystick1Button10) && playerManager.controls.Equals(PlayerManager.Controls.joystick)))
                isBoostActive = true;

        if (boostBar.fillAmount < 1)
            boostBar.fillAmount += boostFillSpeed * Time.deltaTime;

        if (!isMoving && !CheckForMovement())
        {
            isMoving = true;

            if (isBoostActive && !hasMoveSpeedChanged && !disableBoost) //if boost was activated and there is no another change in move speed
                StartCoroutine(boostSnake());
            else if (hasMoveSpeedChanged)
                moveSpeed = moveSpeedChanged;

            if (!isBoostActive && !hasMoveSpeedChanged)
                moveSpeed = defaultMoveSpeed;

            GetMiddleTile();
            GetNeighbourTiles();

            moveTo = myDirection.Equals(Direction.none) ? middleTile : myDirection.Equals(Direction.left) ? leftTile : rightTile;
            head.transform.right = basic.CellToWorld(moveTo) - head.position; //turn snake's head

            MoveSnake();
            StartCoroutine(Move(basic.CellToWorld(moveTo)));

            myDirection = Direction.none;
        }

        if(!disableTurretTurn)
        {
            if (((Input.GetKeyDown(KeyCode.LeftBracket) || Input.GetKeyDown(KeyCode.RightBracket)) && playerManager.controls.Equals(PlayerManager.Controls.keyboard))
            || ((Input.GetKeyDown(KeyCode.Joystick1Button2) || Input.GetKeyDown(KeyCode.Joystick1Button1)) && playerManager.controls.Equals(PlayerManager.Controls.joystick))) //turn turrets
            {
                int direction = (Input.GetKeyDown(KeyCode.LeftBracket) || Input.GetKeyDown(KeyCode.Joystick1Button2)) ? 1 : -1; //get turn direction

                //In Unity, rotate values in inspector are different than rotate values taken from script so we need our own variable for rotation
                turnValue = turnValue >= 390f ? 30f : turnValue <= -390f ? -30f : turnValue; //prevent from values higher than 360 and lower than -360 (prevent from exceeding the range of variable)
                turnValue += direction * 60f; //60f - constant rotate value (60f = rotate hex one time)

                for (int i = 0; i < bodies.childCount; i++) //get all turrets
                    if (bodies.GetChild(i).childCount > 0 && bodies.GetChild(i).GetChild(0).TryGetComponent(out Turret myTurret))
                        myTurret.startRotateTurret(direction);
            }
        }
    }

    private bool CheckForMovement() //write something better in the future to check if all tail parts has reached their new position
    {
        for(int i = 0; i < bodies.childCount; i++)
        {
            if (bodies.GetChild(i).GetComponent<Body>().isMoving)
                return true;
        }

        return false;
    }

    private void MoveSnake() //move snake's head and all body parts
    {
        Vector3 currentPosition = head.position;

        for (int i=0; i < bodies.childCount; i++)
        {
            Vector3 previousTailPart = bodies.GetChild(i).transform.position;
            StartCoroutine(bodies.GetChild(i).GetComponent<Body>().Move(currentPosition, moveSpeed));
            currentPosition = previousTailPart;
        }
    }

    private IEnumerator Move(Vector3 to) //move head to new position
    {
        while (head.position != to)
        {
            head.position = Vector3.MoveTowards(head.position, to, moveSpeed * Time.deltaTime);
            yield return null;
        }

        isMoving = false;
    }

    private void GetMiddleTile()
    {
        Vector3Int headPos = basic.WorldToCell(head.position);
        Vector3Int body = basic.WorldToCell(mainBody.position);

        Vector3Int result;

        int side = head.position.x > mainBody.position.x ? 1 : -1; //get on which side is head (left is -1 and right is 1)

        //explained in my notes
        if (headPos.y == body.y)
        {
            result = new Vector3Int(body.x + (2 * side), body.y, 0); 
            moveTileSide = 1 * side;
        }
        else if (headPos.y < body.y)
        {
            result = new Vector3Int(body.x + side, body.y - 2, 0);
            moveTileSide = 2 * side;
        }
        else
        {
            result = new Vector3Int(body.x + side, body.y + 2, 0);
            moveTileSide = 3 * side;
        }

        middleTile = result;
    }

    private void GetNeighbourTiles()
    {
        Vector2 leftChild, rightChild;
        leftChild = rightChild = Vector2.zero;
        
        int isEven = Mathf.Abs(middleTile.y % 2) == 0 ? 1 : 0; //check if actual y is even
        int isOdd = Mathf.Abs(middleTile.y % 2) == 1 ? 1 : 0; //check if actual y is odd

        switch (moveTileSide)
        {
            case 1:
                leftChild = new Vector2(-1 + isOdd, 1);
                rightChild = new Vector2(-1 + isOdd, -1);
                break;
            case -1:
                leftChild = new Vector2(0 + isOdd, -1);
                rightChild = new Vector2(0 + isOdd, 1);
                break;
            case 2:
                leftChild = new Vector2(1 - isEven, 1);
                rightChild = new Vector2(-1, 0);
                break;
            case -2:
                leftChild = new Vector2(1, 0);
                rightChild = new Vector2(0 - isEven, 1);
                break;
            case 3:
                leftChild = new Vector2(-1, 0);
                rightChild = new Vector2(1 - isEven, -1);
                break;
            case -3:
                leftChild = new Vector2(0 - isEven, -1);
                rightChild = new Vector2(1, 0);
                break;
        }
        
        leftTile = (Vector3Int)Vector2Int.CeilToInt(leftChild) + middleTile;
        rightTile = (Vector3Int)Vector2Int.CeilToInt(rightChild) + middleTile;
    }

    private IEnumerator boostSnake()
    {
        boostBar.fillAmount = 0;

        moveSpeed = boostMoveSpeed;
        yield return new WaitForSeconds(boostTime);
        isBoostActive = false;
    }

    public void ChangeMoveSpeed(float speed)
    {
        moveSpeedChanged = speed;
        hasMoveSpeedChanged = (speed == defaultMoveSpeed) ? false : true;
    }

    public void DisableBoost(bool state)
    {
        disableBoost = state;
    }

    public void DisableTurretTurn(bool state)
    {
        disableTurretTurn = state;
    }

    public void DisableSnakeControl(bool state)
    {
        disableSnakeControl = state;
    }
}
