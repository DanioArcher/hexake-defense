﻿using System.Collections;
using UnityEngine;

/*
 * Script for tail parts
 */

public class Body : MonoBehaviour
{
    private bool isInviolability = false;
    public bool isMoving = false;

    public void TurnOffInviolability() 
    {
        isInviolability = false;
        GetComponent<CircleCollider2D>().enabled = true;
        GetComponent<SpriteRenderer>().color = Color.white;

        if (transform.childCount > 0 && transform.GetChild(0).TryGetComponent(out SpriteRenderer turretSprite))
        {
            turretSprite.GetComponent<Turret>().enabled = true;
            turretSprite.color = Color.white;
        }
    }

    public IEnumerator Move(Vector3 to, float moveSpeed) //move one body part to the next position
    {
        isMoving = true;

        while (transform.position != to)
        {
            transform.position = Vector3.MoveTowards(transform.position, to, moveSpeed * Time.deltaTime);
            yield return null;
        }

        isMoving = false;
    }

    //activate inviolability when player hit another player or an obstacle
    public IEnumerator Inviolability() //Inviolability - nietykalnosc
    {
        GetComponent<CircleCollider2D>().enabled = false;
        isInviolability = true;

        //if there is a turret in this tail part
        if (transform.childCount > 0 && transform.GetChild(0).TryGetComponent(out SpriteRenderer turretSprite)) 
            StartCoroutine(TurretInviolability(turretSprite));

        while (isInviolability) //sprite blinking effect for tail part
        {
            GetComponent<SpriteRenderer>().color = Color32.Lerp(new Color32(255, 255, 255, 255), new Color32(255, 255, 255, 128), Mathf.PingPong(Time.time, 1f));
            yield return null;
        }
    }

    private IEnumerator TurretInviolability(SpriteRenderer turretSprite)
    {
        turretSprite.GetComponent<Turret>().enabled = false; //during inviolability we can not shoot with our turrets

        while (isInviolability) //sprite blinking effect for turret
        {
            turretSprite.color = Color32.Lerp(new Color32(255, 255, 255, 255), new Color32(255, 255, 255, 128), Mathf.PingPong(Time.time, 1f));
            yield return null;
        }
    }
}
