﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerManager : MonoBehaviour
{
    private GameObject head;
    private GameObject body;
    [HideInInspector] public GameObject canvas;
    private GameObject store;
    private GameObject endPanel;

    private int score;

    public GameObject menu;
    public Text messageText;
    public GameObject powerUpsBar;
    public GameObject powerUpSlot;
    private PowerUp powerUp;

    public enum Controls { keyboard, joystick }; //says what controls player is using
    public Controls controls;

    private Text woodText;
    private Text stoneText;
    private Text bronzeText;

    private IEnumerator messageCourutine;

    private int wood;
    public int Wood
    {
        get { return wood; }
        set
        {
            wood = value;
            woodText.text = wood.ToString();
        }
    }
    private int stone;
    public int Stone
    {
        get { return stone; }
        set
        {
            stone = value;
            stoneText.text = stone.ToString();
        }
    }
    private int bronze;
    public int Bronze
    {
        get { return bronze; }
        set
        {
            bronze = value;
            bronzeText.text = bronze.ToString();
        }
    }

    //variables for power ups
    [HideInInspector] public bool disableTurretShoot = false;

    private void Awake()
    {
        if (name == "Snake1")
            score = PlayerPrefs.GetInt(GameManager.PLAYER_ONE_SCORE);
        else
            score = PlayerPrefs.GetInt(GameManager.PLAYER_TWO_SCORE);

        messageText.transform.parent.gameObject.SetActive(false);
        powerUpSlot.SetActive(false);

        canvas = transform.Find("MainCanvas").gameObject;
        endPanel = canvas.transform.Find("EndPanel").gameObject;
        endPanel.SetActive(false);

        Transform resources = canvas.transform.Find("Resources");

        woodText = resources.GetChild(0).GetComponentInChildren<Text>();
        stoneText = resources.GetChild(1).GetComponentInChildren<Text>();
        bronzeText = resources.GetChild(2).GetComponentInChildren<Text>();

        store = canvas.transform.Find("Store").gameObject;
        store.SetActive(false);

        head = transform.Find("Head").gameObject;
        body = transform.Find("Body").gameObject;

        body.transform.GetChild(0).GetComponent<SpriteRenderer>().color = Color.white; //there is some bug that changes color of the first body part to black

        Wood = 10;
        Stone = 10;
        Bronze = 10;
    }

    private void Update()
    {
        //activating store
        if ((Input.GetKeyDown(KeyCode.B) && controls.Equals(Controls.keyboard))
            || (Input.GetKeyDown(KeyCode.Joystick1Button3) && controls.Equals(Controls.joystick))) 
            store.SetActive(!store.activeSelf);

        //activating menu panel
        if((Input.GetKeyDown(KeyCode.Escape) && controls.Equals(Controls.keyboard))
            || (Input.GetKeyDown(KeyCode.JoystickButton7) && controls.Equals(Controls.joystick))) 
            if(GameManager.hasGameStarted && GameManager.isControllerConnected)
                menu.SetActive(!menu.activeSelf);

        //clearing after using the power up
        if((Input.GetKeyDown(KeyCode.Tab) && controls.Equals(Controls.keyboard))
            || (Input.GetKeyDown(KeyCode.Joystick1Button9) && controls.Equals(Controls.joystick)))
        {
            if (powerUp)
            {
                powerUp.StartEffect();
                powerUp = null;
                powerUpSlot.transform.GetChild(0).GetComponent<Image>().sprite = null;
                powerUpSlot.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = "";
                powerUpSlot.SetActive(false);
            }
        }
    }

    public IEnumerator SnakeFlashing() //for turning on and off inviolability for head and all body/tail parts
    {
        StartCoroutine(head.GetComponent<Head>().Inviolability());
        for(int i = 0; i < body.transform.childCount; i++)
            StartCoroutine(body.transform.GetChild(i).GetComponent<Body>().Inviolability());

        yield return new WaitForSeconds(3f);

        head.GetComponent<Head>().TurnOffInviolability();
        for (int i = 0; i < body.transform.childCount; i++)
            body.transform.GetChild(i).GetComponent<Body>().TurnOffInviolability();
    }

    public void SetMovementScript(bool state)
    {
        GetComponent<Movement>().enabled = state;
    }

    public void DisplayMessage(string text) //display message on UI (e.g. "You do not have enough resources to buy this turret")
    {
        if(messageCourutine != null) //player can invoke a message when it is still being displayed - prevent from that
            StopCoroutine(messageCourutine);

        messageCourutine = ShowMessage(text);
        StartCoroutine(messageCourutine);
    }

    private IEnumerator ShowMessage(string text)
    {
        messageText.transform.parent.gameObject.SetActive(true);
        messageText.text = text;
        yield return new WaitForSeconds(3f);
        messageText.transform.parent.gameObject.SetActive(false);
    }

    public void DisableTurretShoot(bool state) //for one of the power ups
    {
        disableTurretShoot = state;
    }

    public bool AddToPowerUpSlot(PowerUp givenPowerUp) //add collected power up to player and display it on UI
    {
        if(!powerUp)
        {
            powerUp = givenPowerUp;
            powerUpSlot.transform.GetChild(0).GetComponent<Image>().sprite = powerUp.image;
            powerUpSlot.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = powerUp.name;
            powerUpSlot.SetActive(true);
            return true;
        }

        return false;
    }

    public void SetEndPanel(bool enable, string text = "") //panel displayed after end of a round
    {
        endPanel.gameObject.SetActive(enable);

        if (enable)
            endPanel.transform.GetChild(0).GetComponent<Text>().text = text;
    }

    public void IncrementScore() => score++;

    private void OnDisable() //when exiting scene, this function also executes
    {
        if (name == "Snake1")
            PlayerPrefs.SetInt(GameManager.PLAYER_ONE_SCORE, score);
        else
            PlayerPrefs.SetInt(GameManager.PLAYER_TWO_SCORE, score);
    }

    public void UpdateScoreText() //update score on UI
    {
        endPanel.transform.GetChild(1).GetComponent<Text>().text = "Score: " + score.ToString();
    }
}
