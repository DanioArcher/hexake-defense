﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

//[Old and unused script] Swiping effect for browsing turrets in shop

public class HorizontalScroll : MonoBehaviour
{
    private ScrollRect scrollRect;
    private int numberOfElements;

    private float newPos = 0f;

    private void Awake()
    {
        scrollRect = GetComponent<ScrollRect>();
        numberOfElements = transform.GetChild(0).childCount;
        scrollRect.horizontalNormalizedPosition = 0;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            StopAllCoroutines();
            scrollRect.horizontalNormalizedPosition = newPos;
            newPos = scrollRect.horizontalNormalizedPosition + ((1f / numberOfElements) + 0.05f);
            if(newPos < 1) StartCoroutine(ScrollHorizontal(newPos));
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            StopAllCoroutines();
            scrollRect.horizontalNormalizedPosition = newPos;
            newPos = scrollRect.horizontalNormalizedPosition - ((1f / numberOfElements) + 0.05f);
            if (newPos > 0) StartCoroutine(ScrollHorizontal(newPos));
        }
    }

    private IEnumerator ScrollHorizontal(float offset)
    {
        while(scrollRect.horizontalNormalizedPosition <= scrollRect.horizontalNormalizedPosition + offset)
        {
            scrollRect.horizontalNormalizedPosition = Mathf.Lerp(scrollRect.horizontalNormalizedPosition, newPos, 5f * Time.deltaTime);
            yield return null;
        }
    }
}
